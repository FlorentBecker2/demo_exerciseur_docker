Exercice java
---------------

Écrivez une classe MaClasseTest contenant une méthode calculer qui renvoie la somme des deux entiers passés en paramètre.

.. easypython:: /exercices/java/
   :language: java
   :titre: Exercice Java
   :extra_yaml:
       nom_classe_test: MaClasseTest
       nom_classe_etu: MaClass
   :nomClasse: MaClass

