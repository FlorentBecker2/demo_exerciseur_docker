Produit de deux entiers
------------------------

Écrivez une fonction ``produit`` prenant en entrée deux entiers et renvoyant le produit des deux entiers.

.. easypython:: /exercices/essai
   :titre: PacagePython
   :language: PackagePython
